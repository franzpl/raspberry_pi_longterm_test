#!/usr/bin/env python3

import socket
import fcntl
import struct

from telegram import Update
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackContext

def  start(update: Update, context: CallbackContext):
        update.message.reply_text(ipAddress)


s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
#connect to any target website
s.connect(('google.com', 0))
ipAddress = s.getsockname()[0]
s.close()

updater = Updater('YOUR_TOKEN')
dispatcher = updater.dispatcher

start_handler = CommandHandler('ip', start)
dispatcher.add_handler(start_handler)

updater.start_polling()


