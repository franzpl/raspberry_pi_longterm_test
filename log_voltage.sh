#!/usr/bin/env bash

sudo stty -F /dev/ttyACM0 9600 raw

#cu -s 9600 -l ttyACM0 &

#kill -9 $! 2>&1 > /dev/null


#TS=$(date+%Y-%m-%d)
TS=$(date +%Y-%m-%d)

#cat /dev/ttyACM0 | ts "%Y%m%d;%H%M%S;" > messung/${TS}_test.csv
#cat /dev/ttyACM0 | tr -s '\n' | ts "%Y%m%d;%H%M%S;" #> /home/pi/Desktop/Measurement/Start_ADC_Data_${TS}.csv
cat /dev/ttyACM0 | ts "%Y%m%d;%H%M%S;" > /home/pi/Desktop/Measurement/Start_ADC_Temp_Data_${TS}.csv
