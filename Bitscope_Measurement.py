#!/usr/bin/env python3

"""Measuring PWM Signal (amp = 100uA, pulsewidth= 60uA, f = 130Hz). """

import numpy as np
import matplotlib.pyplot as plt
import time
import os
from subprocess import call

date = time.strftime('%Y-%m-%d')

newpath = r'/home/pi/Desktop/Measurement/V2.0_Measurement'+'_'+date
if not os.path.exists(newpath):
	os.makedirs(newpath)
	
os.chdir("/home/pi/Desktop/Measurement_Code/")	
		
call(["./runme", "runme"]) # running c program

# Bitscope
bitscope_fs = 1200000 # 1 MHz <-> 1us resolution
bitscope_no_of_samples = 10000 
bitscope_voltage_data = []
trigger = 0.7 # U in V

# Signal
pulsewidth = 60e-6 # 60 us
f = 130 # Hz
T = 1 / f
duty_cycle = pulsewidth / T


crs = open("/home/pi/Desktop/Measurement/V2.0_Measurement"+"_"+date+"/V2.0_Waveform_Data" +"_"+date+'.txt', "r")
for columns in ( raw.strip().split() for raw in crs):
	bitscope_voltage_data.append(columns[0])

bitscope_voltage = np.array(bitscope_voltage_data, dtype=np.float64)
bitscope_voltage_no_offset = bitscope_voltage - 0.035937

try:
	trigger_index = next(x[0] for x in enumerate(bitscope_voltage_no_offset) if x[1] > trigger)
	plt.plot(bitscope_voltage_no_offset[trigger_index - 10 : trigger_index + 80])
	plt.xticks([0, 10, 20, 30, 40, 50, 60, 70, 80])
except:
	
	plt.plot(bitscope_voltage_no_offset)

plt.grid()
plt.title('Stimulus (100$\mu$A @15k$\Omega$, 60$\mu$s, 130Hz) measured by BitScope')
plt.xlabel('t / $\mu$s')
plt.ylabel('U / V')
plt.ylim([-0.25, 1.6])
plt.savefig('/home/pi/Desktop/Measurement/V2.0_Measurement'+'_'+date+'/V2.0_Waveform'+'_'+date+'.pdf')
