#!/usr/bin/env python3

import numpy as np
import sys
import time
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import csv

if len(sys.argv) > 1:
	action = sys.argv[1]
else:
	action =  None

date = time.strftime("%Y-%m-%d")

battery_voltage = []
voltage_drop = []
boost_voltage = []
temp = []

with open('/home/pi/Desktop/Measurement/Start_ADC_Temp_Data'+'_'+'2018-10-16.csv', 'r') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=';')
    i = 0
    for row in csv_reader:
        i = i+1
        if i < 20:
            continue
        battery_voltage.append(row[2])
        voltage_drop.append(row[3])
        boost_voltage.append(row[4])
        temp.append(row[5])

battery_voltage_data = np.array(battery_voltage, dtype=np.float64)
voltage_drop_data = np.array(voltage_drop, dtype=np.float64)
boost_voltage_data = np.array(boost_voltage, dtype=np.float64)
temp_data = np.array(temp, dtype=np.float64)

current_consumption = (battery_voltage_data - voltage_drop_data) / 33.0

battery_voltage_data_smoothed = np.around(battery_voltage_data, decimals=5)
current_consumption_smoothed = np.around(current_consumption * 1e6)
boost_voltage_data_smoothed = np.around(boost_voltage_data, decimals=5)

filename_ADC_dynamic = '/home/pi/Desktop/Measurement/V2.0_Measurement'+'_'+date+'/V2.0_ADC_Data_Dynamic'+'_'+date+'.pdf'
filename_ADC_xlim = '/home/pi/Desktop/Measurement/V2.0_Measurement'+'_'+date+'/V2.0_ADC_Data'+'_'+date+'.pdf'
filename_ADC_Temp_dynamic = '/home/pi/Desktop/Measurement/V2.0_Measurement'+'_'+date+'/Battery_Voltage_vs_Temp_Dynamic'+'_'+date+'.pdf'
filename_ADC_Temp_xlim = '/home/pi/Desktop/Measurement/V2.0_Measurement'+'_'+date+'/Battery_Voltage_vs_Temp'+'_'+date+'.pdf'

fig, ax1 = plt.subplots()
plt.grid()
ax1.set_title('V2.0 Long-Term Experiment (CR2032)', fontsize='x-large')
ax1.plot(np.arange(len(battery_voltage_data_smoothed)) / 360, battery_voltage_data_smoothed, 'b-', label='Battery Voltage')
ax1.plot(np.arange(len(battery_voltage_data_smoothed)) / 360, boost_voltage_data_smoothed, 'g-', label='Boost Voltage')
ax1.set_ylim(0, 3.5)
ax1.yaxis.set_ticks([3.3, 3.0, 2.0, 1.0])
ax1.set_xlabel('t / h')
ax1.set_ylabel('U / V')

if action == '--set_xlim':
	ax1.set_xlim(0, 1300)
	filename = filename_ADC_xlim
elif action is None:
	filename = filename_ADC_dynamic


ax2 = ax1.twinx()
ax2.plot(np.arange(len(battery_voltage_data_smoothed)) / 360, current_consumption_smoothed, 'r-', label='Current Drain')
ax2.set_ylabel('I / $\mu$A', color='r')
ax2.set_ylim(0, 300)
ax2.tick_params('y', colors='r')
fig.legend(prop={'size': 6})

fig.savefig(filename, bbox_inches='tight')

fig.clear()

fig, ax3 = plt.subplots()
plt.grid()
ax3.set_title('Battery Voltage (CR2032) vs. Temperature', fontsize='x-large')
ax3.plot(np.arange(len(battery_voltage_data_smoothed)) / 360, battery_voltage_data_smoothed, 'b-', label='Battery Voltage')
ax3.set_xlabel('t / h')
ax3.set_ylabel('U / V', color='b')
ax3.tick_params('y', colors='b')

if action == '--set_xlim':
	ax3.set_xlim(0, 1300)
	ax3.set_ylim(0, 3.3)
	ax3.yaxis.set_ticks([3.0, 2.0, 1.0])
	filename = filename_ADC_Temp_xlim
	ax4 = ax3.twinx()
	ax4.set_ylim(0, 40)
elif action is None:
	filename = filename_ADC_Temp_dynamic
	ax4 = ax3.twinx()

ax4.plot(np.arange(len(temp_data)) / 360, temp_data, 'purple', label='Temperature')
ax4.set_ylabel(r'$\Theta$ / $\circ$C', color='purple')
ax4.tick_params('y', colors='purple')
fig.legend(prop={'size': 6})

fig.savefig(filename, bbox_inches='tight')
